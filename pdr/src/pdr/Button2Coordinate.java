package pdr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ボタン押下のラベルデータから，時刻-三次元座標のファイルを生成します．
 * 第一引数：歩行センシングデータのルートのディレクトリパス
 * 第二引数：***-coordinate-meter-rel.txtの入っているディレクトリのパス
 * 
 * @author kaji
 *
 */
public class Button2Coordinate {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		Map<String, List<String>> cmap=new HashMap<String, List<String>>();
		//System.out.println(args[0]);
		File root=new File(args[0]);
		
		File cdir=new File(args[1]);
		for(File f:cdir.listFiles()){
			if(!f.getName().endsWith("-coordinate-meter-rel.txt"))continue;
			String rname=f.getName().substring(0,f.getName().indexOf("-"));
			//System.out.println(rname);
			BufferedReader in=new BufferedReader(new FileReader(f));
			String line;
			List<String> list=new ArrayList<String>();
			while((line=in.readLine())!=null){
				if(line.indexOf(",")==-1)continue;
				list.add(line);
				//System.out.println(line);
			}
			in.close();
			//System.out.println(rname);
			//System.out.println(list.size());
			
			cmap.put(rname, list);
			
		}
		
		
		for(File route:root.listFiles()){
			if(route.isFile())continue;
			String rname=route.getName();
			//System.out.println(rname);
			List<String> list=cmap.get(rname);
			
			for(File dir:route.listFiles()){
				if(dir.isFile())continue;
				
				for(File f:dir.listFiles()){
					if(!f.getName().endsWith(".label"))continue;
										
					BufferedReader in=new BufferedReader(new FileReader(f));
					PrintWriter out=new PrintWriter(new File(f.getParent(),"coordinate-correct.txt"));
					String line;
					int index=0;
					while((line=in.readLine())!=null){
						if(line.indexOf("BTN")==-1)continue;
						line=line.replaceAll(",BTN", list.get(index));
						index++;
						out.println(line);
						out.flush();
					}
					
					in.close();
					out.close();
					
				}
			}
		}
	}

}
